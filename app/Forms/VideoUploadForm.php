<?php

namespace CodeFlix\Forms;

use Kris\LaravelFormBuilder\Form;

class VideoUploadForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('thumb', 'file', [
                'label' => 'Thumbnail',
                'required' => false,
                'rules' => 'image|max:1024'
            ])
            ->add('file', 'file',[
                'label' => 'Arquivo de vídeo',
                'required' => false,
                'rules' => 'mimetypes:video/mp4'
            ])->add('duration', 'text',[
                'label' => 'Duração',
                'rules' => 'required|integer|min:1'
            ]);
    }
}
