<?php
/**
 * Created by PhpStorm.
 * User: Programador
 * Date: 12/05/2017
 * Time: 08:37
 */

namespace CodeFlix\Media;


trait VideoPaths
{
    use ThumbPaths;

    /**
     * @return string
     */
    public function getThumbFolderStorageAttribute()
    {
        return "videos/{$this->id}";
    }

    /**
     * @return string
     */
    public function getFileFolderStorageAttribute()
    {
        return "videos/{$this->id}";
    }

    /**
     * @return string
     */
    public function getFileAssetAttribute()
    {
        return $this->isLocalDriver() ? route('admin.videos.file_asset', ['video' => $this->id]) :
            $this->file_path;
    }

    /**
     * @return mixed
     */
    public function getThumbDefaultAttribute()
    {
        return env('VIDEO_NO_THUMB');
    }

     /**
     * @return string
     */
    public function getFileRelativeAttribute()
    {
        return $this->file ? "{$this->file_folder_storage}/{$this->file}" : false;
    }

    /**
     * @return \Illuminate\Filesystem\FilesystemAdapter
     */
    public function getFilePathAttribute()
    {
        if(!$this->file_relative) {
            return false;
        }
        return $this->getAbsolutePath($this->getStorage(), $this->file_relative);
    }

    /**
     * @return string
     */
    public function getThumbAssetAttribute()
    {
        return $this->isLocalDriver() ?
            route('admin.videos.thumb_asset', ['video' => $this->id]) : $this->thumb_path;
    }

    /**
     * @return string
     */
    public function getThumbSmallAssetAttribute()
    {
        return $this->isLocalDriver() ?
            route('admin.videos.thumb_small_asset', ['video' => $this->id]) : $this->thumb_small_path;
    }

}