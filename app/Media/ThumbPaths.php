<?php
/**
 * Created by PhpStorm.
 * User: Programador
 * Date: 16/05/2017
 * Time: 08:20
 */

namespace CodeFlix\Media;


trait ThumbPaths
{
    use VideoStorages;

    /**
     * @return string
     */
    public function getThumbRelativeAttribute()
    {
        return $this->thumb ? "{$this->thumb_folder_storage}/{$this->thumb}" : false;
    }

    /**
     * @return \Illuminate\Filesystem\FilesystemAdapter
     */
    public function getThumbPathAttribute()
    {
        if(!$this->thumb_relative) {
            return false;
        }

        return $this->getAbsolutePath($this->getStorage(), $this->thumb_relative);
    }

    /**
     * @return string
     */
    public function getThumbSmallRelativeAttribute()
    {
        if(!$this->thumb) {
            return false;
        }

        list($name, $extension) = explode('.', $this->thumb);
        return "{$this->thumb_folder_storage}/{$name}_small.{$extension}";
    }

    /**
     * @return \Illuminate\Filesystem\FilesystemAdapter
     */
    public function getThumbSmallPathAttribute()
    {
        if(!$this->thumb_relative) {
            return false;
        }
        return $this->getAbsolutePath($this->getStorage(), $this->thumb_small_relative);
    }

}