<?php

namespace CodeFlix\Media;


use Illuminate\Filesystem\FilesystemAdapter;

trait VideoStorages
{

    /**
     * Storage
     */
    public function getStorage()
    {
        return \Storage::disk($this->getDiskDriver());
    }

    /**
     * @return mixed
     */
    protected function getDiskDriver()
    {
        return config('filesystems.default');
    }

    /**
     * @param FilesystemAdapter $storage
     * @param $fileRelativePath
     * @return FilesystemAdapter
     */
    protected function getAbsolutePath(FilesystemAdapter $storage, $fileRelativePath)
    {
        return $this->isLocalDriver() ? $storage->getDriver()->getAdapter()->applyPathPrefix($fileRelativePath) : $storage->url($fileRelativePath);
    }

    /**
     * @return bool
     */
    public function isLocalDriver()
    {
        $driver = config("filesystems.disks.{$this->getDiskDriver()}.driver");
        return $driver == 'local';
    }

}