import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {JwtCredentials} from "../models/jwt-credentials";
import {Storage} from "@ionic/storage";
import {AuthHttp, JwtHelper} from "angular2-jwt";
import {Env} from "../models/env";

declare var ENV:Env;

@Injectable()
export class JwtClient {

    private _token = null;
    private _payload = null;

    /**
     *
     * @param authHttp
     * @param storage
     * @param jwtHelper
     */
    constructor(
        public authHttp: AuthHttp,
        public storage: Storage,
        public jwtHelper: JwtHelper
    ){
        this.getToken();
        this.getPayload().then((payload) => {
            console.log(payload);
        });
    }

    /**
     *
     * @returns {Promise<T>}
     */
    getPayload(): Promise<Object> {
        return new Promise((resolve) => {
            if(this._payload) {
                resolve(this._payload);
            }

            this.getToken().then((token) => {
                if(token) {
                    this._payload = this.jwtHelper.decodeToken(token);
                }

                resolve(this._payload);
            });
        });
    }

    /**
     *
     * @returns {Promise<T>}
     */
    getToken(): Promise<string> {
        // resolve = resolucao da promessa
        return new Promise((resolve) => {
            if(this._token) {
                resolve(this._token);
            }

            this.storage.get(ENV.TOKEN_NAME).then((token) => {
                this._token = token;
                resolve(this._token);
            });
        });

    }

    /**
     *
     * @param JwtCredentials
     * @returns {Promise<TResult2|TResult1>|Promise<TResult>|Promise<TResult|Response>|Promise<Response>}
     */
    accessToken(jwtCredentials: JwtCredentials): Promise<string> { // type hint do tipo que retorna
        return this.authHttp.post(`${ENV.API_URL}/access_token`, jwtCredentials)
            .toPromise()
            .then((response: Response) => {
                // ao invés de var usar o let que é acessível apenas no escopo local
                let token = response.json().token;
                this._token = token;
                this.storage.set(ENV.TOKEN_NAME, this._token);

                return token;
            });
    }

    /**
     *
     *
     */
    revokeToken(): Promise<null> {
        let headers = new Headers();
        headers.set('Authorization', `Bearer ${this._token}`);
        let requestOptions = new RequestOptions({headers});

        return this.authHttp.post(`${ENV.API_URL}/logout`, {}, requestOptions)
            .toPromise()
            .then((response: Response) => {
                // ao invés de var usar o let que é acessível apenas no escopo local
                this._token = null;
                this._payload = null;
                this.storage.clear();

                return null;
            });
    }

}
