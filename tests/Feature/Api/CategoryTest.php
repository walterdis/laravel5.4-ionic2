<?php

namespace Tests\Feature\Api;

use CodeFlix\Models\User;
use Dingo\Api\Auth\Auth;
use Dingo\Api\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Model;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tymon\JWTAuth\JWT;
use Tymon\JWTAuth\JWTGuard;

class CategoryTest extends TestCase
{
    use DatabaseMigrations;


    /**
     *
     */
    public function testCategoriesIndex()
    {
        $response = $this->makeJWTToken();
        $token = $response->json()['token'];

        $testResponse = $this->get('api/categories', [
            'Authorization' => "Bearer $token"
        ])->assertStatus(200);
    }

    /**
     *
     */
    protected function makeJWTToken()
    {
        $urlGenerator = app(UrlGenerator::class)->version('v1');

        return $this->post($urlGenerator->route('api.access_token'), [
            'email' => 'admin@user.com',
            'password' => 'secret'
        ]) ;


    }

}
