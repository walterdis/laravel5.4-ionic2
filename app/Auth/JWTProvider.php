<?php
/**
 * Created by PhpStorm.
 * User: Deiks
 * Date: 23/05/2017
 * Time: 19:33
 */

namespace CodeFlix\Auth;


use Dingo\Api\Auth\Provider\Authorization;
use Dingo\Api\Routing\Route;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWT;

class JWTProvider extends Authorization
{

    /**
     * @var JWT
     */
    private $jwt;

    function __construct(JWT $jwt)
    {
        $this->jwt = $jwt;
    }

    /**
     * Get the providers authorization method.
     *
     * @return string
     */
    public function getAuthorizationMethod()
    {
        return 'bearer';
    }

    /**
     * Authenticate the request and return the authenticated user instance.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Dingo\Api\Routing\Route $route
     *
     * @return mixed
     */
    public function authenticate(Request $request, Route $route)
    {
        try {
            return \Auth::guard('api')->authenticate();

        } catch (AuthenticationException $exception) {
            $this->refreshToken();
            return \Auth::guard('api')->user();
        }
    }

    private function refreshToken()
    {
        $token = $this->jwt->parseToken()->refresh();

        $this->jwt->setToken($token);
    }


}