<?php
/**
 * Created by PhpStorm.
 * User: Deiks
 * Date: 13/05/2017
 * Time: 12:12
 */

namespace CodeFlix\Media;


use Folklore\Image\Facades\Image;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\UploadedFile;
use Imagine\Image\Box;

trait VideoUploads
{
    use Uploads;

    /**
     * @param $id
     * @param UploadedFile $file
     * @return mixed
     */
    public function uploadFile($id, UploadedFile $file)
    {
        $model = $this->find($id);
        $name = $this->upload($model, $file, 'file');

        if($name) {
            $this->deleteFileOld($model);
            $model->file = $name;
            $model->save();
        }

        return $model;
    }

    /**
     * @param $model
     */
    public function deleteFileOld($model)
    {
        $storage = $model->getStorage();
        if($storage->exists($model->file_relative)) {
            $storage->delete([$model->file_relative]);
        }
    }

}