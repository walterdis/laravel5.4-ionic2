<?php
/**
 * Created by PhpStorm.
 * User: Programador
 * Date: 17/05/2017
 * Time: 08:57
 */

namespace CodeFlix\Media;


use Illuminate\Http\UploadedFile;

trait Uploads
{
/**
     * @param $model
     * @param UploadedFile $file
     * @return false|string
     */
    public function upload($model, UploadedFile $file, $type)
    {
        /** @var FilesystemAdapter $storage */
        $storage = $model->getStorage();
        $name = md5(time() . "{$model->id}-{$file->getClientOriginalName()}") . ".{$file->getClientOriginalExtension()}";

        $result = $storage->putFileAs($model->{"{$type}_folder_storage"}, $file, $name);
        return $result ? $name : $result;
    }
}