<?php


namespace CodeFlix\Media;


trait SeriePaths
{
    use ThumbPaths;

    /**
     * @return string
     */
    public function getThumbFolderStorageAttribute()
    {
        return "series/{$this->id}";
    }

    /**
     * @return string
     */
    public function getThumbAssetAttribute()
    {
        return $this->isLocalDriver() ? route('admin.series.thumb_asset', ['serie' => $this->id]) :
            $this->thumb_path;
    }

    /**
     * @return string
     */
    public function getThumbSmallAssetAttribute()
    {
        return $this->isLocalDriver() ? route('admin.series.thumb_small_asset', ['serie' => $this->id]) :
            $this->thumb_small_path;

    }

    /**
     * @return mixed
     */
    public function getThumbDefaultAttribute()
    {
        return env('SERIE_NO_THUMB');
    }
}