NPM
- npm install {pacote@versao}  --save para salvar no package.json

PHP STORM:
- File -> Settings -> Plugins | laravel plugin, 
- File -> Settings -> Languages & Frameworks -> PHP -> Laravel | Enable plugin
- 


LARAVEL
- php artisan app:name CodeFlix
- php artisan ide-helper:generate (ativer o fluent)
composer require doctrine/dbal:~2.3
- php artisan ide-helper:models --dir="app/Models" CodeFlix\Models\User Category ... (deixar vazio pega todos)
- alt + j para selecionar verticalmente do mesmo tipo

Notifications:
- php artisan make:notification "nome" - cria um arquivo de notification.
- php artisan vendor:publish --tag=laravel-notifications


FormBuilder:
- php artisan make:form Forms/Userform --fields="name:text, email:email"


Laravel Mix
- npm install (instala do packages.json)
- npm run dev (roda servico dev no webpack.js)

Repository:
- php artisan vendor:publish --provider=" Prettus\Repository\Providers\RepositoryServiceProvider"
- php artisan make:repository 
