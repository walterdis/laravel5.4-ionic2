<?php

namespace CodeFlix\Forms;

use Kris\LaravelFormBuilder\Form;

class UserSettingsForm extends Form
{
    public function buildForm()
    {
        $id = $this->getData('id');
        $this /*
            ->add('password_old', 'password', [
                'label' => 'Senha antiga',
                'rules' => "required|max:255|exists:users,password"
            ])
            */
            ->add('password', 'password', [
                'label' => 'Nova senha',
                #'rules' => "required|max:255, $id"
                'rules' => "required|confirmed|min:3|max:16"
            ])->add('password_confirmation', 'password', [
                'label' => 'Repita a senha',
            ]);
    }
}
