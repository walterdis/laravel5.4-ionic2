<?php

namespace Tests\Browser;

use CodeFlix\Models\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CategoryTest extends DuskTestCase
{

    use DatabaseMigrations;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $user = User::where('email', '=', 'admin@user.com')->first();

        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit(route('admin.categories.index'))
                ->assertSee('Listagem de Categorias')
                ->clickLink('Nova Categoria')
                ->assertSee('Nova Categoria')
                ->type('name', 'test')
                ->click('button[type=submit]')
                ->assertSee('Listagem de Categorias')
                ->assertSee('test')

                ->visit(route('admin.categories.edit', ['id' => 1]))
                ->assertSee('Editar Categoria')
                ->type('name', 'teste editado')
                ->click('button[type=submit]')
                ->assertSee('Listagem de Categorias')
                ->visit(route('admin.categories.show', ['id' => 1]))
                ->assertSee('Ver categoria')
                ->press('.btn-danger')
                ->assertSee('Categoria excluída')
            ;
        });
    }



}
