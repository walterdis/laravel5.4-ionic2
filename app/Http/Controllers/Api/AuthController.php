<?php

namespace CodeFlix\Http\Controllers\Api;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use CodeFlix\Http\Controllers\Controller;

class AuthController extends Controller
{

    use AuthenticatesUsers;

    /**
     * @param Request $request
     */
    public function accessToken(Request $request)
    {
        $this->validateLogin($request);
        $credentials = $this->credentials($request);

        if($token = \Auth::guard('api')->attempt($credentials)) {
            return $this->sendLoginResponse($request, $token);
        }

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * @param Request $request
     * @param $token
     * @return array
     */
    public function sendLoginResponse(Request $request, $token)
    {
        return ['token' => $token];
    }

    /*
     *
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return response()->json([
            'error' => \Lang::get('auth.failed')
        ], 400);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        \Auth::guard('api')->logout();
        return response()->json([], 204); // Resposta de sucesso sem conteúdo
    }

    /**
     * @param Request $request
     * @return array
     */
    public function refreshToken(Request $request)
    {
        $token = \Auth::guard('api')->refresh();
        return $this->sendLoginResponse($request, $token);
    }
}
