<?php

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Seeder;

class SeriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var Collection $series */
        $series = factory(\CodeFlix\Models\Serie::class, 5)->create();

        $repository = app(\CodeFlix\Repositories\SerieRepository::class);
        $collectionThumbs = $this->getThumbs();

        $series->each(function($serie) use($repository, $collectionThumbs) {
            $repository->uploadThumb($serie->id, $collectionThumbs->random());
        });

    }

    /**
     * @return \Illuminate\Support\Collection
     */
    protected function getThumbs()
    {
        return new \Illuminate\Support\Collection([
            new \Illuminate\Http\UploadedFile(
                storage_path('app/files/faker/thumbs/thumb_symphony.png'), 'thumb_symphony.png'
            )
        ]);
    }
}
