<?php

namespace CodeFlix\Http\Controllers\Admin\Auth;

use CodeFlix\Forms\UserSettingsForm;
use CodeFlix\Repositories\UserRepository;
use Illuminate\Http\Request;
use CodeFlix\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\Form;

class UserSettingsController extends Controller
{
    //

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * PasswordController constructor.
     * @param UserRepository $repository
     */
    function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        $form = \FormBuilder::create(UserSettingsForm::class, [
            'url' => route('admin.user_settings.edit'),
            'method' => 'PUT'
        ]);


        return view('admin.auth.settings', compact('form'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return $this
     */
    public function update(Request $request)
    {
        $id = \Auth::id();

        /** @var Form $form */
        $form = \FormBuilder::create(UserSettingsForm::class, [
            'data' => ['id' => $id]
        ]);

        if(!$form->isValid()) {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $data = $form->getFieldValues();
        $this->repository->update($data, $id);

        $request->session()->flash('message', 'Senha atualizada!');

        return redirect()->route('admin.users.index');

    }
}
